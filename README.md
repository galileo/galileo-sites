# GALILEO Sites

A collection of CSV files, and the source files (xls, html, pdf, etc.) from which the CSV data was derived.

## General

In general, the idea for this project is to keep track of the source of the sites data we enter into GALILEO Admin, e.g., K-12 Schools, Public Library branches, etc.

## Source to CSV

The source of the specific `sites` CSV data is GALILEO Admin (the table report).

The other CSV data is produced by converting xls, html, and pdf table data to CSV using various (sometimes online) tools.

Once converted, the data in the CSV files stored here have be manually formatted for easier reading.

Newly converted documents may need this kind of manual manipulation.

## Example Google Spreadsheet

_Sites/K-12 Schools Reconciliation_

https://docs.google.com/spreadsheets/d/1x6xjYsWQAByPHExapPlPSkzhdFdFziV_SRJG240AD6A/edit?usp=sharing

_Copy of DataRequest-DistrictAndSchools2023-24_

https://docs.google.com/spreadsheets/d/1M-SK3wYEUkh1SrqIo7K8FT1S0y1UEtCHkxKrroxgh0s/edit?usp=sharing
