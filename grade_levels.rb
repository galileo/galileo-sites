#!/usr/bin/env ruby

require 'csv'
require 'json'
STDOUT.sync = true

#---------------------------------------------------------------------
# main:

# puts JSON.pretty_generate( {} )

public_csv  = "csvs/public.csv"
charter_csv = "csvs/charter.csv"
grade_levels_csv = "csvs/grade_levels.csv"

public_hash = public_schools( public_csv )
charter_hash = charter_schools( charter_csv )
merge_csv( public_hash, charter_hash, grade_levels_csv )

#---------------------------------------------------------------------
BEGIN {

#---------------------------------------------------------------------
def merge_csv( public_hash, charter_hash, grad_levels_csv )
  grade_levels = []
  seen = {}
  headers = %w[school_id grade_level site_type]
  CSV.open( grad_levels_csv, 'w' ) do |csv|
    csv << headers
    public_hash.each do |school_id, hash|
      seen[ school_id ] = true
      grade_level = hash[:grade_range]
      site_type   = hash[:site_type]
      csv << [ school_id, grade_level, site_type ]
    end
    charter_hash.each do |school_id, hash|
      next if seen[ school_id ]
      grade_level = hash[:grade_range]
      site_type   = hash[:site_type]
      csv << [ school_id, grade_level, site_type ]
    end
  end
end

#---------------------------------------------------------------------
def site_type( grade_level )
  site_type = 'k12'
  return site_type if grade_level =~ /,/
  (lower, upper) = grade_level.split(/-/)
  if grade_level == "PK-05"
    site_type = 'elem'
  elsif grade_level == "K-5"
    site_type = 'elem'
  elsif grade_level == "06-08"
    site_type = 'midd'
  elsif grade_level == "6-8"
    site_type = 'midd'
  elsif grade_level == "09-12"
    site_type = 'highschool'
  elsif grade_level == "9-12"
    site_type = 'highschool'
  elsif upper =~ /K/
    site_type = 'elem'
  elsif upper.to_i <= 5
    site_type = 'elem'
  end
  site_type
end

def public_schools( csv_in )
  public_hash = {}
  # School Year,District ID,District Name,School ID,School Name,Grade Range,School Address,School City,State,School ZIPCODE,Mailing Address,Mailing City,Mailing ZIPCODE,Principal Name,Email Address,School Phone,School Fax
  CSV.foreach( csv_in, headers: true ) do |row|
    district_id = row['District ID']
    school_id   = row['School ID']
    grade_range = row['Grade Range']
    id = sprintf( "%03d-%04d", district_id, school_id )
    site_type = site_type( grade_range )
    public_hash[ id ] = {
      grade_range: grade_range,
      site_type: site_type
    }
  end
  public_hash
end

def charter_schools( csv_in )
  charter_hash = {}
  # Type,Count,Name,County / District Served,System Code,School Code,Year Charter Opened,Charter Current Term,Charter End,Grades Served in 2023-24
  CSV.foreach( csv_in, headers: true ) do |row|
    district_id = row['System Code']
    school_id   = row['School Code']
    grade_range = row['Grades Served in 2023-24']
    id = sprintf( "%03d-%04d", district_id.to_i, school_id.to_i )
    site_type = site_type( grade_range )
    charter_hash[ id ] = {
      grade_range: grade_range,
      site_type: site_type
    }
  end
  charter_hash
end

#---------------------------------------------------------------------
}  # BEGIN

__END__
