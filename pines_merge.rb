#!/usr/bin/env ruby

require 'csv'
require 'json'
STDOUT.sync = true

#---------------------------------------------------------------------
# main:

# puts JSON.pretty_generate( {} )

PATCH = {
  "Aaron Heard Resource Center"     => "East Athens Community Center",
  "Blakely-Maddox Memorial Library" => "Lucy Maddox Memorial Library",
  "Midway/Riceboro Library"         => "Midway-Riceboro Library",
  "Rome-Floyd County Library"       => "Rome/Floyd County Library",
  "Willis L. Miller Library"        => "Valdosta-Lowndes County Library",
  "Headquarters, Statesboro"        => "Statesboro-Bulloch County Library",
  "Ochlocknee Public Library"       => "Gladys M. Clark Public Library",
  "Mount Zion Public Library"       => "Mt. Zion Public Library"
}

pines_branches_csv    = "csvs/pines_branches.csv"
publiclibs_sites_csv  = "csvs/sites_publiclib_2023_11_06.csv"
merged_pines_csv      = "csvs/merged_pines.csv"
institution_pines_csv = "csvs/institution-pines.csv"

(pines_branches_hash, pines_system_hash) = pines_branches( pines_branches_csv )
institution_pines_hash                   = institution_pines( institution_pines_csv )
(publiclibs_sites_hash, inst_hash)       = publiclibs_sites( publiclibs_sites_csv )
name_matches_hash = name_matches( pines_system_hash, publiclibs_sites_hash, institution_pines_hash, inst_hash )
merge_csv( name_matches_hash, institution_pines_hash, publiclibs_sites_hash, pines_branches_csv, merged_pines_csv )

#---------------------------------------------------------------------
BEGIN {

#---------------------------------------------------------------------
def merge_csv ( name_hash, inst_hash, sites_hash, csv_in, csv_out )
  site_headers = %w[institution_code institution_name site_code new_code site_name new_name site_type charter change]
  CSV.open( csv_out, 'w' ) do |csv|
    csv << site_headers
    # name,code
    CSV.foreach( csv_in, headers: true ) do |row|
      ret = merge_row( row, name_hash, inst_hash, sites_hash )
      csv << ret unless ret.nil?
    end
    count = 0
    sites_hash.each do |site_name, shash|
      if !shash[:merged]
        csv << [ shash[:instcode], shash[:instname], shash[:site_code], '', site_name, '', shash[:site_type], shash[:charter], '3-none' ]
        count += 1
      end
    end
    puts "unmerged: #{count}"
  end
end

def merge_row ( row, name_hash, inst_hash, sites_hash )
  pines_name = row['name']
  pines_code = row['code'].downcase
  if pines_name == "Northwest Branch" && pines_code =~ /claytn-nw/
    pines_name = "#{pines_name} (Clayton County)"
  end
  sites_name = name_hash[ pines_name ] || pines_name
  ret = nil
  if (shash = sites_hash[ pines_name ])
    sites_hash[ pines_name ][:merged] = true
    new_code = "#{shash[:instcode]}:#{pines_code}"
    ret = [ shash[:instcode], shash[:instname], shash[:site_code], new_code, sites_name, pines_name, shash[:site_type], shash[:charter], '2-update' ]
  elsif (shash = sites_hash[ sites_name ])
    sites_hash[ sites_name ][:merged] = true
    new_code = "#{shash[:instcode]}:#{pines_code}"
    ret = [ shash[:instcode], shash[:instname], shash[:site_code], new_code, sites_name, pines_name, shash[:site_type], shash[:charter], '2-update' ]
  else
    system_code = pines_code.downcase.split(/-/).first
    if (ihash = inst_hash[ system_code ])
      new_code = "#{ihash[:instcode]}:#{pines_code}"
      ret = [ ihash[:instcode], ihash[:instname], '', new_code, '', pines_name, 'publiclib', 'false', '1-add' ]
    else
      ret = [ '', '', '', pines_code, pines_name, 'publiclibs', 'false', '1-add' ]
    end
  end
  ret
end

#---------------------------------------------------------------------
def publiclibs_sites( csv_in )
  sites_hash = {}
  inst_hash = {}
  # institution_code,institution_name,code,name,site_type,charter
  CSV.foreach( csv_in, headers: true ) do |row|
    instcode = row['institution_code']
    name     = row['name']
    sites_hash[ name ] = {
      instcode:  instcode,
      instname:  row['institution_name'],
      site_code: row['code'],
      site_type: row['site_type'],
      charter:   row['charter'],
      merged:    false
    }
    inst_hash[ instcode ] ||= []
    inst_hash[ instcode ] << name
  end
  [ sites_hash, inst_hash ]
end

#---------------------------------------------------------------------
def institution_pines( csv_in )
  inst_hash = {}
  # id,code,public_code,name,open_athens_org_id,pines_codes
  CSV.foreach( csv_in, headers: true ) do |row|
    pines_code = row['pines_codes'].sub(/\["/,'').sub(/"\]/,'').downcase
    inst_hash[ pines_code ] = {
      instcode:   row['code'],
      instname:   row['name']
    }
  end
  inst_hash
end

#---------------------------------------------------------------------
def pines_branches( csv_in )
  pines_hash = {}
  system_hash = {}
  # name,code
  CSV.foreach( csv_in, headers: true ) do |row|
    pines_code = row['code'].downcase
    pines_name = row['name']
    pines_system_code = pines_code.split(/-/).first
    pines_hash[ pines_code ] = {
      name:   pines_name,
      system: pines_system_code
    }
    system_hash[ pines_system_code ] ||= []
    system_hash[ pines_system_code ] << pines_name
  end
  [ pines_hash, system_hash ]
end

#---------------------------------------------------------------------
def name_matches( pines_system_hash, publiclist_sites_hash, institution_pines_hash, inst_hash )
  name_matches_hash = {}
  pines_system_hash.each do |pines_system, pines_names|
    instcode = institution_pines_hash[ pines_system ][:instcode]
    sites_names = inst_hash[ instcode ]
    next unless sites_names
    matched = {}
    pines_names.sort.each do |pines_name|
      if (sites_name = PATCH[ pines_name ])
        name_matches_hash[ pines_name ] = sites_name
        matched[ sites_name ] = true
        next  # next pines_name
      end
      sites_names.sort.each do |sites_name|
        next if matched[ sites_name ]
        match = false
        if pines_name == sites_name
          match = true
        elsif pines_name.split(/ /).first == sites_name.split(/ /).first
          match = true
        elsif sites_name =~ /\b#{pines_name.split(/ /).first}\b/
          match = true
        elsif pines_name =~ /\b#{sites_name.split(/ /).first}\b/
          match = true
        end
        if match
          name_matches_hash[ pines_name ] = sites_name
          matched[ sites_name ] = true
          break  # next pines_name
        end
      end
    end
  end
  name_matches_hash
end

#---------------------------------------------------------------------
}  # BEGIN

__END__

