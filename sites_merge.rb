#!/usr/bin/env ruby

require 'csv'
require 'json'
STDOUT.sync = true

#---------------------------------------------------------------------
# main:

existing_sites_csv = "csvs/sites_k12_2023_11_06.csv"
sites_to_merge_csv = "csvs/Sites_Schools Merge Report - working copy.csv"
merged_sites_csv   = "csvs/merged_sites.csv"

existing_sites_hash = existing_sites( existing_sites_csv )
merge_csv( existing_sites_hash, sites_to_merge_csv, merged_sites_csv )

#---------------------------------------------------------------------
BEGIN {

#---------------------------------------------------------------------
def merge_csv ( sites_hash, csv_in, csv_out )
  site_headers = %w[institution_code institution_name code new_code name site_type charter change]
  CSV.open( csv_out, 'w' ) do |csv|
    csv << site_headers
    # name,site_code,new_code,school_id,system,inst,site_type,charter,notes
    CSV.foreach( csv_in, headers: true ) do |row|
      ret = merge_row( row, sites_hash )
      csv << ret unless ret.nil?
    end
    count = 0
    sites_hash.each do |site_code, shash|
      if !shash[:merged]
        csv << [ shash[:instcode], shash[:instname], site_code, '', shash[:name], shash[:site_type], shash[:charter], '3-none' ]
        count += 1
      end
    end
    puts "unmerged: #{count}"
  end
end

def merge_row ( row, sites_hash )
  site_code = row['site_code']
  new_code  = row['new_code']
  school_id = row['school_id']
  ret = nil
  if site_code.nil?
    if new_code.nil?
      # skip these -- hand merged in spreadsheet (yellow)
    else
      instcode = new_code.split(/:/)[0]
      instname = row['system']
      name = row['name']
      (site_type, charter) = site_type( name )
      if instname == "Atlanta Public Schools"
        name += " (#{instname})"
      elsif instname == "Dalton Public Schools"
        name += " (#{instname})"
      elsif instname == "Chatham County Schools"
        name += " (Savannah-Chatham County)"
      elsif instname =~ /^(.*) Schools/
        suffix = $1
        name += " (#{suffix})"
      end
      # new site -- not matched in spreadsheet (green)
      ret = [ instcode, row['system'], '', new_code, name, site_type, charter, '1-add' ]
    end
  else  # site_code not nil
    shash = sites_hash[ site_code ]
    if new_code.nil?
      # no change -- not matched in spreadsheet (blue)
      sites_hash[ site_code ][:merged] = true
      ret = [ shash[:instcode], shash[:instname], site_code, '',        shash[:name], shash[:site_type], shash[:charter], '3-none' ]
    else
      # new code -- matched in spreadsheet (white)
      sites_hash[ site_code ][:merged] = true
      ret = [ shash[:instcode], shash[:instname], site_code, new_code,  shash[:name], shash[:site_type], shash[:charter], '2-update' ]
    end
  end
  ret
end

#---------------------------------------------------------------------
def site_type( name )
  site_type = 'k12'
  charter   = false
  if name =~ /elementary|primary/i
    site_type = 'elem'
  elsif name =~ /middle school/i
    site_type = 'midd'
  elsif name =~ /junior high school/i
    site_type = 'midd'
  elsif name =~ /high school/i
    site_type = 'highschool'
  end
  if name =~ /charter/i
    charter = true
  end
  [ site_type, charter ]
end

#---------------------------------------------------------------------
def existing_sites( csv_in )
  sites_hash = {}
  # institution_code,institution_name,code,name,site_type,charter
  CSV.foreach( csv_in, headers: true ) do |row|
    sites_hash[ row['code'] ] = {
      instcode:  row['institution_code'],
      instname:  row['institution_name'],
      name:      row['name'],
      site_type: row['site_type'],
      charter:   row['charter'],
      merged:    false
    }
  end
  sites_hash
end

#---------------------------------------------------------------------
}  # BEGIN

__END__

