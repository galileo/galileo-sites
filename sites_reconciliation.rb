#!/usr/bin/env ruby

require 'csv'
require 'json'
STDOUT.sync = true

#---------------------------------------------------------------------
# main:

puts "csv_out"
csv_out
puts "merge_schools"
merge_hash = merge_schools
puts "merge report"
merge_report( merge_hash )

#---------------------------------------------------------------------
BEGIN {

#---------------------------------------------------------------------
def merge_report( merge_hash )
  file = 'csvs/sites_merge_report.csv'
  headers = %w[name site_code new_code school_id system inst site_type charter]
  seen = {}
  CSV.open( file, 'w' ) do |csv|
    csv << headers
    merge_hash.each do |key, arr|
      (name, site_code, new_code, school_id, system, inst, site_type, charter) = parse_site( arr )
      csv << [name, site_code, new_code, school_id, system, inst, site_type, charter]
      seen[school_id] = true
    end
    school_table.each do |name, hash|
      school_id = hash[:school_code]
      system = hash[:system]
      system += " Schools" if system.match(/ (?:County|City)$/)
      system = "Decatur City Schools" if system == "City Schools of Decatur"
      system = "Chatham County Schools" if system == "Savannah-Chatham County Schools"
      unless seen[school_id]
         csv << [name, '', '', school_id, system, '', '', '']
      end
    end
  end
end

#---------------------------------------------------------------------
def parse_site( site_arr )
  abort("no site?") unless site_arr[0][0] == "sites"
  name      = site_arr[0][1]
  site_code = site_arr[0][2]
  system    = site_arr[0][3]
  inst      = site_arr[0][4]
  site_type = site_arr[0][5]
  charter   = site_arr[0][6]
  new_code  = ''
  if site_arr.size > 1
    school_id = ''
    site_arr[1 .. -1].each do |arr|
      if arr[0] == "sites"
        puts "dupl site: #{site_code}"
        next
      end
      abort("not agree? #{site_code}") if school_id.length > 0 && school_id != arr[2]
      school_id = arr[2]
      new_code = "#{inst}:#{school_id}"
    end
  end
  return [name, site_code, new_code, school_id, system, inst, site_type, charter]
end

#---------------------------------------------------------------------
def merge_schools
  file = 'json/sites_reconciliation.json'
  merge_hash = {}
  matched = {}
  unmatched = {}
  sit_t = sites_table
  pub_t = public_table
  pri_t = private_table
  cha_t = charter_table
  sys_t = system_table
  sch_t = school_table
  puts "merge sites"
  sit_t.each do |name, hash|
    nk = name_key( name.sub(/ \([^)]+\)$/){}, hash[:system] )
    if ['k12','elem','midd','highschool'].include? hash[:site_type]
      merge_hash[ nk ] ||= []
      merge_hash[ nk ] << ['sites',name,hash[:school_code],hash[:system],hash[:system_code],hash[:site_type],hash[:charter]]
    end
  end
  puts "merge public"
  pub_t.each do |name, hash|
    nk = name_key( name, hash[:system] )
    if merge_hash.has_key?( nk )
      matched[ nk ] = true
      merge_hash[ nk ] << ['public',name,hash[:school_code],hash[:system],hash[:system_code]]
    end
  end
  puts "merge school"
  sch_t.each do |name, hash|
    nk = name_key( name, hash[:system] )
    if merge_hash.has_key?( nk )
      matched[ nk ] = true
      merge_hash[ nk ] << ['school',name,hash[:school_code],hash[:system],hash[:system_code]]
    end
  end
#   puts "merge private"
#   pri_t.each do |name, hash|
#     nk = name_key( name, hash[:system] )
#     if merge_hash.has_key?( nk )
#       matched[ nk ] = true
#       merge_hash[ nk ] << ['private',name,hash[:school_code],hash[:system],hash[:system_code]]
#     end
#   end
  puts "merge charter"
  cha_t.each do |name, hash|
    nk = name_key( name, hash[:system] )
    if merge_hash.has_key?( nk )
      matched[ nk ] = true
      merge_hash[ nk ] << ['charter',name,hash[:school_code],hash[:system],hash[:system_code]]
    end
  end
  puts "set public"
  pub_t.each do |name, hash|
    nk = name_key( name, hash[:system] )
    matched[ nk ] = false unless matched.has_key?( nk )
  end
  puts "set school"
  sch_t.each do |name, hash|
    nk = name_key( name, hash[:system] )
    matched[ nk ] = false unless matched.has_key?( nk )
  end
#   puts "set private"
#   pri_t.each do |name, hash|
#     nk = name_key( name, hash[:system] )
#     matched[ nk ] = false unless matched.has_key?( nk )
#   end
  puts "set charter"
  cha_t.each do |name, hash|
    nk = name_key( name, hash[:system] )
    matched[ nk ] = false unless matched.has_key?( nk )
  end
#   puts "check merge"
#   merge_hash.each do |name, arr|
#     if arr.size < 2
#       puts name
#       # puts JSON.pretty_generate( merge_hash[name] )
#     end
#   end
  puts "write json"
  File.write( file, JSON.pretty_generate( [merge_hash, matched] ) )
  merge_hash
end

#---------------------------------------------------------------------
# creates a csv file for human review, e.g., brainstorming
def csv_out
  file = 'csvs/sites_reconciliation.csv'
  headers = ['source','name_key','name','code','system','system_code']
  CSV.open( file, 'w' ) do |csv|
    csv << headers
    puts "csv public"
    public_table.each do |name, hash|
      csv << ['public', name_key(name), name, hash[:school_code], hash[:system], hash[:system_code]]
    end
    puts "csv school"
    school_table.each do |name, hash|
      csv << ['school', name_key(name), name, hash[:school_code], hash[:system], hash[:system_code]]
    end
    puts "csv charter"
    charter_table.each do |name, hash|
      csv << ['charter', name_key(name), name, hash[:school_code], hash[:system], hash[:system_code]]
    end
    puts "csv private"
    private_table.each do |name, hash|
      csv << ['private', name_key(name), name, hash[:school_code], hash[:system], hash[:system_code]]
    end
    puts "csv sites"
    sites_table.each do |name, hash|
      n = name.sub(/ \([^)]+\)$/){}
      if( ['k12','elem','midd','highschool'].include? hash[:site_type] )
        csv << ['sites', name_key(n), name, hash[:school_code], hash[:system], hash[:system_code]]
      end
    end
  end
end

#---------------------------------------------------------------------
# SystemID,SchoolID,SystemName,SchoolName
def school_table
  file  = 'csvs/school.csv'
  table = {}
  CSV.foreach( file, headers: true ) do |row|
    name = row['SchoolName']
    school_code = row['SchoolID']
    system = row['SystemName']
    system_code = row['SystemID']
    table[ name ] = { system: system, system_code: system_code, school_code: school_code }
  end
  table
end

#---------------------------------------------------------------------
# SystemID,SystemName
def system_table
  file  = 'csvs/system.csv'
  table = {}
  CSV.foreach( file, headers: true ) do |row|
    system = row['SystemName']
    system_code = row['SystemID']
    table[ system ] = { system: system, system_code: system_code }
  end
  table
end

#---------------------------------------------------------------------
# School Year,District ID,District Name,School ID,School Name,Grade Range,School Address,School City,State,School ZIPCODE,Mailing Address,Mailing City,Mailing ZIPCODE,Principal Name,Email Address,School Phone,School Fax
def public_table
  file  = 'csvs/public.csv'
  table = {}
  CSV.foreach( file, headers: true ) do |row|
    name = row['School Name']
    system = row['District Name']
    system_code = row['District ID']
    system_code = system_code[0..2] if system_code.length > 3
    school_code = "#{system_code}-#{sprintf("%04d", row['School ID'].to_i)}"
    table[ name ] = { system: system, system_code: system_code, school_code: school_code }
  end
  table
end

#---------------------------------------------------------------------
# Type,Count,Name,County / District Served,System Code,School Code,Year Charter Opened,Charter Current Term,Charter End,Grades Served in 2023-24
def charter_table
  file = 'csvs/charter.csv'
  table = {}
  CSV.foreach( file, headers: true ) do |row|
    name = row['Name']
    system = row['County / District Served']
    system_code = row['System Code']
    school_code = "#{system_code}-#{sprintf("%04d", row['School Code'].to_i)}"
    table[ name ] = { system: system, system_code: system_code, school_code: school_code }
  end
  table
end

#---------------------------------------------------------------------
# Count,School ID,Name,Address,City,State,Zip,Private School County Location,GA County/City responsible for Contact Info:,Profit/Non-profit Status,Name of business,Business ID,Business State,501c3 Status,501c3 Employer ID,Telephone,Email,Web Address
def private_table
  file = 'csvs/private.csv'
  table = {}
  CSV.foreach( file, headers: true ) do |row|
    name = row['Name']
    (system_code, system) = row['GA County/City responsible for Contact Info:'].split('-',2)
    system = row['Private School County Location'] if system == ""
    school_code = "#{system_code}-#{sprintf("%04d", row['School ID'].to_i)}"
    table[ name ] = { system: system, system_code: system_code, school_code: school_code }
  end
  table
end

#---------------------------------------------------------------------
# institution_code,institution_name,code,name,site_type,charter
def sites_table
  file   = 'csvs/sites.csv'
  boolean = { 'true' => true, 'false' => false }
  table = {}
  CSV.foreach( file, headers: true ) do |row|
    name = row['name']
    system = row['institution_name']
    system_code = row['institution_code']
    school_code = row['code']
    site_type = row['site_type']
    charter = boolean[ row['charter'] ]
    table[ name ] = { system: system, system_code: system_code, school_code: school_code, site_type: site_type, charter: charter }
  end
  table
end

#---------------------------------------------------------------------
# id,code,public_code,name,active,institution_group_id,eds_profile_teen,eds_profile_high_school
def institution_table
  file = 'csvs/institution.csv'
  groups = { '3' => 'k12', '10' => 'k12ttc', '9' => 'privk12' }
  table = {}
  CSV.foreach( file, headers: true ) do |row|
    name = row['name']
    code = row['code']
    type = groups[ row['institution_group_id'] ]
    table[ code ] = { name: name, type: type }
  end
  table
end

#---------------------------------------------------------------------
def name_key( name, system='' )
  n = name.dup
  if system != ''
    first = system.split(/ /)[0]
    first = 'Chatham' if first == 'Savannah-Chatham'
    n += " - #{first}"
  end
  n.downcase!
  n.gsub!(/[^a-zA-Z0-9]/){' '}
  while n =~ /  /
    n.gsub!(/  /){' '}
  end
  n
end

#---------------------------------------------------------------------
}  # BEGIN

__END__

